/**
 * Created by g15oit18 on 21.11.2017.
 */
public class Main {
    public static void main(String[] args) {
        Synchronization synchronization1 = new Synchronization();
        Synchronization synchronization2 = new Synchronization();
        synchronization1.start();
        synchronization2.start();
    }
}
